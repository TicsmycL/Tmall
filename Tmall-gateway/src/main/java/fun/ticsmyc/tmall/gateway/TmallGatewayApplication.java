package fun.ticsmyc.tmall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author Ticsmyc
 * @date 2020-04-29 18:59
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class TmallGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(TmallGatewayApplication.class);
    }
}
