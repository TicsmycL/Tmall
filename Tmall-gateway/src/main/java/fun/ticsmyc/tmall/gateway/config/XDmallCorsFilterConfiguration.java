package fun.ticsmyc.tmall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 跨域
 *
 * @author Ticsmyc
 * @date 2020-04-29 19:00
 */
@Configuration
public class XDmallCorsFilterConfiguration {

    @Bean
    public CorsFilter corsFilter(){
        //初始化配置对象
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //添加允许跨域的域名 ,可以设置多个, *代表所有域名。 如果要携带cookie，则不能设为*
//        corsConfiguration.addAllowedOrigin("http://manage.ticmall.com");
        corsConfiguration.addAllowedOrigin("*");
        //允许携带cookie
        corsConfiguration.setAllowCredentials(true);
        //允许所有的请求方法跨域访问
        corsConfiguration.addAllowedMethod("*");
        //允许携带所有头信息跨域访问
        corsConfiguration.addAllowedHeader("*");

        //初始化配置源对象
        UrlBasedCorsConfigurationSource configurationSource = new UrlBasedCorsConfigurationSource();
        //拦截所有路径，校验是否允许跨域
        configurationSource.registerCorsConfiguration("/**",corsConfiguration);
        return new CorsFilter(configurationSource);
    }
}
