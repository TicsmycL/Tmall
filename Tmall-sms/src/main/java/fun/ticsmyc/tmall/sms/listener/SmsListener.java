package fun.ticsmyc.tmall.sms.listener;

import fun.ticsmyc.tmall.sms.config.SmsProperties;
import fun.ticsmyc.tmall.sms.utils.SmsUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.mail.MessagingException;
import java.security.GeneralSecurityException;
import java.util.Map;

/**
 * @author Ticsmyc
 * @date 2020-05-14 10:35
 */
@Component
@EnableConfigurationProperties(SmsProperties.class)
public class SmsListener {

    @Autowired
    private SmsUtils smsUtils;

    @RabbitListener(bindings = @QueueBinding(
            value=@Queue(value="XDMALL.SMS.VERIFY.QUEUE",durable = "true"),
            exchange=@Exchange(value="XDMALL.SMS.EXCHANGE",ignoreDeclarationExceptions = "true",type= ExchangeTypes.TOPIC),
            key={"sms.verify"}
    ))
    public void verifyListener(Map<String,String> msg){
        if(CollectionUtils.isEmpty(msg)){
            return;
        }
        String email = msg.get("email");
        String code = msg.get("code");

        if(StringUtils.isNotBlank(email) && StringUtils.isNotBlank(code)){
            if(!smsUtils.emailFormat(email)){
                return;
            }
            try {
                smsUtils.sendEmail(email,"西电二手商城验证码","验证码："+code);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    @RabbitListener(bindings = @QueueBinding(
            value=@Queue(value="XDMALL.SMS.WELCOME.QUEUE",durable = "true"),
            exchange=@Exchange(value="XDMALL.SMS.EXCHANGE",ignoreDeclarationExceptions = "true",type= ExchangeTypes.TOPIC),
            key={"sms.welcome"}
    ))
    public void welcomeListener(Map<String,String> msg){
        if(CollectionUtils.isEmpty(msg)){
            return;
        }
        String email = msg.get("email");
        String name = msg.get("name");
        if(StringUtils.isNotBlank(email) && StringUtils.isNotBlank(name)){
            if(!smsUtils.emailFormat(email)){
                return;
            }
            try {
                smsUtils.sendEmail(email,"欢迎使用西电二手商城",name+" 你好，欢迎使用西电二手商城，后续订单等消息都通过该邮箱发送~");
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    @RabbitListener(bindings = @QueueBinding(
            value=@Queue(value="XDMALL.SMS.ORDER.QUEUE",durable = "true"),
            exchange=@Exchange(value="XDMALL.SMS.EXCHANGE",ignoreDeclarationExceptions = "true",type= ExchangeTypes.TOPIC),
            key={"sms.order"}
    ))
    public void orderListener(Map<String,String> msg){
        if(CollectionUtils.isEmpty(msg)){
            return;
        }
        String email = msg.get("email");
        String userName = msg.get("userName");
        String goodsName = msg.get("goodsName");
        String buyerName = msg.get("buyerName");
        String buyerPhone = msg.get("buyerPhone");
        if(StringUtils.isNotBlank(email) && StringUtils.isNotBlank(goodsName) && StringUtils.isNotBlank(userName)){
            if(!smsUtils.emailFormat(email)){
                return;
            }
            try {
                smsUtils.sendEmail(email,"西电二手商城商品预约提醒",userName+" 你好，你发布的商品“"+goodsName+
                        "”已被"+buyerName+"预约。 对方的联系方式为："+buyerPhone+"，请及时沟通。详情请查看西电二手交易小程序。");
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    @RabbitListener(bindings = @QueueBinding(
            value=@Queue(value="XDMALL.SMS.COMMON.QUEUE",durable = "true"),
            exchange=@Exchange(value="XDMALL.SMS.EXCHANGE",ignoreDeclarationExceptions = "true",type= ExchangeTypes.TOPIC),
            key={"sms.common"}
    ))
    public void commonListener(Map<String,String> msg){
        if(CollectionUtils.isEmpty(msg)){
            return;
        }
        String email = msg.get("email");
        String title = msg.get("title");
        String content = msg.get("content");
        if(StringUtils.isNotBlank(email) && StringUtils.isNotBlank(content) && StringUtils.isNotBlank(title)){
            if(!smsUtils.emailFormat(email)){
                return;
            }
            try {
                smsUtils.sendEmail(email,title,content);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }
}
