package fun.ticsmyc.tmall.sms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Ticsmyc
 * @date 2020-05-14 10:36
 */
@ConfigurationProperties(prefix = "xdmall.sms")
public class SmsProperties {
    private String authCode;
    private String fromEmail;

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
}

