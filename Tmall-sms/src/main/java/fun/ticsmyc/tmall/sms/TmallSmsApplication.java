package fun.ticsmyc.tmall.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ticsmyc
 * @date 2020-05-14 10:34
 */
@SpringBootApplication
public class TmallSmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(TmallSmsApplication.class);
    }
}
