package fun.ticsmyc.tmall.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Ticsmyc
 * @date 2020-04-29 19:03
 */
@SpringBootApplication
@EnableDiscoveryClient
public class TmallUploadApplication {
    public static void main(String[] args) {
        SpringApplication.run(TmallUploadApplication.class);
    }
}
