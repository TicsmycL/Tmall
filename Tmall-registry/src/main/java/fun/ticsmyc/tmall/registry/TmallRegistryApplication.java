package fun.ticsmyc.tmall.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Ticsmyc
 * @date 2020-04-29 18:57
 */
@SpringBootApplication
@EnableEurekaServer
public class TmallRegistryApplication {
    public static void main(String[] args) {
        SpringApplication.run(TmallRegistryApplication.class);
    }
}
