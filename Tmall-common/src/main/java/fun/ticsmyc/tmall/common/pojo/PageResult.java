package fun.ticsmyc.tmall.common.pojo;

import java.util.List;

/**
 * @author Ticsmyc
 * @date 2020-04-29 21:36
 */
public class PageResult<T> {


    //当前页内容
    private List<T> items;
    //总页数
    private Integer totalPage;
    //内容数
    private Long total;

    public PageResult() {
    }

    public PageResult(List<T> items, Integer totalPage, Long total) {
        this.items = items;
        this.totalPage = totalPage;
        this.total = total;
    }

    public PageResult(List<T> items, Long total) {
        this.items = items;
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
