package fun.ticsmyc.tmall.common.utils;

import org.apache.commons.codec.digest.DigestUtils;

import org.apache.commons.lang3.StringUtils;


import java.util.UUID;

public class CodecUtils {


    /**
     * MD5加密
     * @param data  要加密的数据
     * @param salt  加密的盐
     * @return
     */
    public static String md5Hex(String data,String salt) {
        if (StringUtils.isBlank(salt)) {
            salt = data.hashCode() + "";
        }
        return DigestUtils.md5Hex(salt + DigestUtils.md5Hex(data));
    }

    public static String shaHex(String data, String salt) {
        if (StringUtils.isBlank(salt)) {
            salt = data.hashCode() + "";
        }
        return DigestUtils.sha512Hex(salt + DigestUtils.sha512Hex(data));
    }

    /**
     * 生成salt
     * @return
     */
    public static String generateSalt(){
        return StringUtils.replace(UUID.randomUUID().toString(), "-", "");
    }
}
