package fun.ticsmyc.tmall.item.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 邮件通知服务
 * @author Ticsmyc
 * @date 2020-05-15 18:46
 */
@Service
public class MessageService {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void sendEmail(String toEmail, String title, String content){
        Map msg = new HashMap<>();
        msg.put("email",toEmail);
        msg.put("title",title);
        msg.put("content",content);
        this.amqpTemplate.convertAndSend("XDMALL.SMS.EXCHANGE","sms.common",msg);
    }
}
