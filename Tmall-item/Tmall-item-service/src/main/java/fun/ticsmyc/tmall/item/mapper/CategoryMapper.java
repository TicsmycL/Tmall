package fun.ticsmyc.tmall.item.mapper;

import fun.ticsmyc.tmall.item.pojo.Category;
import tk.mybatis.mapper.additional.idlist.SelectByIdListMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Ticsmyc
 * @date 2020-04-29 21:46
 */
public interface CategoryMapper extends Mapper<Category>, SelectByIdListMapper<Category,Long> {

}
