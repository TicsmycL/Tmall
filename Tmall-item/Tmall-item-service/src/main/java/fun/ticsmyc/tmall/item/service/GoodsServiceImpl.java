package fun.ticsmyc.tmall.item.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import fun.ticsmyc.rpc.server.annotation.TRPCService;
import fun.ticsmyc.tmall.common.pojo.PageResult;
import fun.ticsmyc.tmall.item.api.GoodsService;
import fun.ticsmyc.tmall.item.mapper.GoodsMapper;
import fun.ticsmyc.tmall.item.pojo.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @author Ticsmyc
 * @date 2020-04-30 19:05
 */
@TRPCService(group="default")
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 根据商品类别查询商品信息
     * @param cid
     * @return
     */
    public PageResult<Goods> queryItemByCid(Long cid, Integer page, Integer rows,Integer status) {
        PageHelper.startPage(page,rows);
        Goods record = new Goods();
        record.setCid(cid);
        if(status !=null){
            record.setStatus(status);
        }
        List<Goods> goods = goodsMapper.select(record);
        PageInfo<Goods> pageInfo = new PageInfo<>(goods);

        return new PageResult<Goods>(goods,pageInfo.getPages(),pageInfo.getTotal());
    }

    /**
     * 添加商品
     * @param goods
     */
    public void addGoods(Goods goods) {
        goods.setCreatetime(new Date());
        goods.setStatus(1);
        this.goodsMapper.insert(goods);
    }

    /**
     * 修改商品
     * @param goods
     */
    public void modifyGoods(Goods goods) {
        goods.setCreatetime(new Date());
        this.goodsMapper.updateByPrimaryKey(goods);
    }

    /**
     * 删除商品
     * @param id
     */
    public void deleteGoods(Long id) {
        Goods goods = this.goodsMapper.selectByPrimaryKey(id);
        if(goods != null){
            this.goodsMapper.deleteByPrimaryKey(id);
        }
    }

    /**
     * 根据id查询商品
     * @param id
     * @return
     */
    public Goods queryItemById(Long id) {
        return this.goodsMapper.selectByPrimaryKey(id);
    }

    /**
     * 查询所有商品
     * @return
     */
    public PageResult<Goods> queryAllItem(Integer page, Integer rows,Integer status) {

        PageHelper.startPage(page,rows);
        List<Goods> goods = null;
        if(status == null){
            goods = this.goodsMapper.selectAll();
        }else{
            Goods record = new Goods();
            record.setStatus(status);
            goods = this.goodsMapper.select(record);
        }
        PageInfo<Goods> pageInfo = new PageInfo<Goods>(goods);

        return new PageResult<Goods>(goods,pageInfo.getPages(),pageInfo.getTotal());
    }

    /**
     * 更新商品状态
     * @param goodsId
     * @param status
     */
    public void updateGoodsStatus(Long goodsId , Integer status){
        Goods record = this.goodsMapper.selectByPrimaryKey(goodsId);
        record.setStatus(status);
        this.goodsMapper.updateByPrimaryKey(record);
    }

    /**
     * 根据用户id  查询该用户发布的商品
     * @param userId
     * @return
     */
    public PageResult<List<Goods>> selectItemsByUserId(Long userId,Integer page, Integer rows,Integer status) {
        PageHelper.startPage(page,rows);
        Goods record = new Goods();
        record.setUserid(userId);
        record.setStatus(status);
        List<Goods> goods = this.goodsMapper.select(record);
        PageInfo pageInfo = new PageInfo(goods);
        return new PageResult<>(pageInfo.getList(),pageInfo.getPages(),pageInfo.getTotal());
    }

    /**
     * 搜索， 结果分页
     * @param content
     * @param page
     * @param row
     */
    public PageResult<Goods> searchGoods(String content,Integer page,Integer row) {
        PageHelper.startPage(page,row);

        Example example = new Example(Goods.class);
        example.orderBy("createtime");
        Example.Criteria criteria = example.createCriteria();

        String[] str= content.split(" ");
        for(String s : str){
            criteria.orLike("title","%"+s+"%");
        }

        List<Goods> goods = this.goodsMapper.selectByExample(example);
        PageInfo<Goods> pageInfo = new PageInfo<>(goods);

        return new PageResult<Goods>(pageInfo.getList(),pageInfo.getPages(),pageInfo.getTotal());
    }
}
