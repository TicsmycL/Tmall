package fun.ticsmyc.tmall.item.mapper;

import fun.ticsmyc.tmall.item.pojo.XdOrder;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Ticsmyc
 * @date 2020-05-14 13:37
 */
public interface OrderMapper extends Mapper<XdOrder> {
}
