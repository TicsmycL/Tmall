package fun.ticsmyc.tmall.item.controller;

import fun.ticsmyc.tmall.common.pojo.PageResult;
import fun.ticsmyc.tmall.item.pojo.XdOrder;
import fun.ticsmyc.tmall.item.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单
 * @author Ticsmyc
 * @date 2020-05-14 13:36
 */
@Deprecated
@Controller
@RequestMapping("order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 预定商品
     * @param userId
     * @param goodsId
     * @return
     */
    @PostMapping
    public ResponseEntity<Void> createOrder(@RequestParam("userId")Long userId,@RequestParam("goodsId")Long goodsId){
        if(userId==null || goodsId==null){
            return ResponseEntity.badRequest().build();
        }
        Boolean flag = this.orderService.createOrder(userId,goodsId);
        if(!flag){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }


    /**
     * 完成订单 【确认收货】
     * @return
     */
    @PutMapping
    public ResponseEntity<Void> finishOrder(@RequestParam("orderId")Long orderId){
        if(orderId == null){
            return ResponseEntity.badRequest().build();
        }
        this.orderService.finishOrder(orderId);

        return ResponseEntity.ok().build();

    }

    /**
     * 取消订单【终止交易】
     * @return
     */
    @DeleteMapping("cancel/{orderId}")
    public ResponseEntity<Void> cancelOrder(@PathVariable("orderId")Long orderId){
        if(orderId == null){
            return ResponseEntity.badRequest().build();
        }
        this.orderService.cancelOrder(orderId);
        return ResponseEntity.ok().build();
    }

    /**
     * 删除订单【交易成功后，可以选择删除order】
     * @return
     */
    @DeleteMapping("delete/{orderId}")
    public ResponseEntity<Void> deleteOrder(@PathVariable("orderId")Long orderId){
        if(orderId == null){
            return ResponseEntity.badRequest().build();
        }
        this.orderService.deleteOrder(orderId);
        return ResponseEntity.ok().build();
    }



    /**
     * 分页查询 “我的订单” 【我买到的商品】
     */
    @GetMapping("/buyer/{userId}")
    public ResponseEntity<PageResult<List<XdOrder>>> selectOrdersByUserId(
            @PathVariable("userId")Long userId,
            @RequestParam(name="page",defaultValue = "1") Integer page,
            @RequestParam(name="rows",defaultValue = "5")Integer rows,
            @RequestParam(name="status",required = false)Integer status
    ){
        if(userId == null){
            return ResponseEntity.badRequest().build();
        }
        PageResult<List<XdOrder>> orders = this.orderService.selectOrdersByUserId(userId,page,rows,status);
        if(CollectionUtils.isEmpty(orders.getItems())){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(orders);
    }

    /**
     * 根据商品id查询订单
     * @param goodsId
     * @return
     */
    @GetMapping("goods/{goodsId}")
    public ResponseEntity<XdOrder> selectOrderByGoodsId(@PathVariable("goodsId")Long goodsId){
        if(goodsId == null){
            return ResponseEntity.badRequest().build();
        }
        XdOrder xdOrder = this.orderService.selectOrderByGoodsId(goodsId);
        if(xdOrder == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(xdOrder);
    }

}
