package fun.ticsmyc.tmall.item.mapper;

import fun.ticsmyc.tmall.item.pojo.CommentReply;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Ticsmyc
 * @date 2020-06-03 22:45
 */
public interface CommentReplyMapper extends Mapper<CommentReply> {
}
