package fun.ticsmyc.tmall.item.dto;

import fun.ticsmyc.tmall.item.pojo.CommentReply;
import fun.ticsmyc.tmall.item.pojo.User;

/**
 * @author Ticsmyc
 * @date 2020-06-04 11:31
 */
public class CommentReplyAndUser {
    private CommentReply commentReply;
    private User user;

    public CommentReply getCommentReply() {
        return commentReply;
    }

    public void setCommentReply(CommentReply commentReply) {
        this.commentReply = commentReply;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
