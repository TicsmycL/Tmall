package fun.ticsmyc.tmall.item.controller;

import fun.ticsmyc.tmall.common.pojo.PageResult;
import fun.ticsmyc.tmall.item.pojo.User;
import fun.ticsmyc.tmall.item.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 用户
 * @author Ticsmyc
 * @date 2020-05-02 21:40
 */
@Controller
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 使用用户id查询用户
     * @param userId
     * @return
     */
    @GetMapping("{userId}")
    public ResponseEntity<User> queryUserById(@PathVariable("userId")Long userId){
        if(userId == null){
            return ResponseEntity.badRequest().build();
        }
        User user = this.userService.queryUserByid(userId);
        if(user == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }

    /**
     * 验证用户名是否已被注册
     * @param username
     * @return
     */
    @GetMapping("/check/{username}")
    public ResponseEntity<Boolean> checkUserName(@PathVariable("username")String username){
        if(StringUtils.isBlank(username)){
            return ResponseEntity.badRequest().build();
        }
        Boolean b = this.userService.checkUserName(username);
        if(b==null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(b);
    }

    /**
     * 获取验证码
     * @param email
     * @return
     */
    @GetMapping("code")
    public ResponseEntity<Void> getVerifyCode(@RequestParam("email")String email){
        this.userService.getVerifyCode(email);
        return ResponseEntity.ok().build();
    }

    /**
     * 用户注册
     * @param user
     * @param code
     * @return
     */
    @PostMapping("register")
    public ResponseEntity<Void> register(User user ,@RequestParam("code") String code){
        if(!this.userService.checkUserName(user.getUsername())){
            //用户名重复
            return ResponseEntity.badRequest().build();
        }
        if(!this.userService.register(user,code)){
            //验证码不正确
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    /**
     * 微信小程序端保存用户信息
     * @param user
     * @return
     */
    @PostMapping("/wx/register")
    public ResponseEntity<Void> wxRegister(User user){
        if(user == null){
            return ResponseEntity.badRequest().build();
        }
        if(!this.userService.wxRegister(user)){
            //用户名冲突 ？
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    /**
     * 微信小程序端修改用户信息
     * @param user
     * @return
     */
    @PutMapping("/wx/modify")
    public ResponseEntity<Void> wxModifyData(User user){
        if(user == null){
            return ResponseEntity.badRequest().build();
        }
        this.userService.wxModifyData(user);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/wx/{openid}")
    public ResponseEntity<User> wxLogin(@PathVariable("openid")String openid){
        User user = this.userService.wxLogin(openid);
        if(user == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }


    /**
     * 登陆校验
     * @param username
     * @param password
     * @return
     */
    @GetMapping("check")
    public ResponseEntity<User> loginCheck(@RequestParam("username")String username,
                                           @RequestParam("password")String password){
        User user = this.userService.loginCheck(username,password);
        if(user == null){
            //密码错误 登陆失败
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.ok(user);
    }


    /**
     * 分页查询所有用户
     * @param page
     * @param rows
     * @return
     */
    @GetMapping("all")
    public ResponseEntity<PageResult<User>> queryAllUser(
            @RequestParam(value = "page",defaultValue = "1")Integer page,
            @RequestParam(value="rows",defaultValue = "5")Integer rows
    ){
        PageResult<User> users = this.userService.queryAllUser(page,rows);
        return ResponseEntity.ok(users);
    }



}
