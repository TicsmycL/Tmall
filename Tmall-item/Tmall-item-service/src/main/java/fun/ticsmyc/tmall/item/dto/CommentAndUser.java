package fun.ticsmyc.tmall.item.dto;

import fun.ticsmyc.tmall.item.pojo.Comment;
import fun.ticsmyc.tmall.item.pojo.User;

/**
 * 为了方便前端渲染，将评论和发评论的用户信息一起返回
 * @author Ticsmyc
 * @date 2020-06-03 19:32
 */
public class CommentAndUser {
    private Comment comment;
    private User user;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}
