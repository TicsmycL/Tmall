package fun.ticsmyc.tmall.item.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import fun.ticsmyc.tmall.common.pojo.PageResult;
import fun.ticsmyc.tmall.item.dto.CommentReplyAndUser;
import fun.ticsmyc.tmall.item.mapper.CommentMapper;
import fun.ticsmyc.tmall.item.mapper.CommentReplyMapper;
import fun.ticsmyc.tmall.item.pojo.Comment;
import fun.ticsmyc.tmall.item.pojo.CommentReply;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ticsmyc
 * @date 2020-05-26 11:47
 */
@Service
public class CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private CommentReplyMapper commentReplyMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 插入一条评论
     * @param comment
     */
    public void insertComment(Comment comment) {
        this.commentMapper.insert(comment);
    }

    /**
     * 分页查询某商品的评论
     * @param goodsId
     * @param page
     * @param row
     * @return
     */
    public PageResult<Comment> selectCommentsByPage(Long goodsId, Integer page, Integer row) {
        PageHelper.startPage(page,row);

        Example example = new Example(Comment.class);
        example.orderBy("id").asc();

        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("goodsId",goodsId);

        List<Comment> comments = this.commentMapper.selectByExample(example);
        PageInfo<Comment> pageInfo = new PageInfo<>(comments);
        return new PageResult<Comment>(pageInfo.getList(),pageInfo.getPages(),pageInfo.getTotal());
    }

    /**
     * 修改评论
     * @param commentId
     * @param content
     */
    public void modifyCommentById(Long commentId,String content) {
        this.commentMapper.updateCommentById(commentId,content);
    }

    /**
     * 根据评论id删除评论
     * @param commentId
     */
    public void deleteCommentById(Long commentId) {
        this.commentMapper.deleteByPrimaryKey(commentId);
    }

    /**
     * 回复评论
     * @param commentReply
     */
    public void insertReplyComment(CommentReply commentReply) {
        this.commentReplyMapper.insert(commentReply);
    }

    /**
     * 删除回复
     * @param replyId
     */
    public void deleteCommentReplyById(Long replyId) {
        this.commentReplyMapper.deleteByPrimaryKey(replyId);
    }

    /**
     * 根据评论id，分页查询这条评论的回复
     * @param commentId
     * @param page
     * @param row
     */
    public PageResult<CommentReplyAndUser> selectCommentsReplyByPage(Long commentId, Integer page, Integer row) {
        PageHelper.startPage(page,row);

        CommentReply record = new CommentReply();
        record.setReply_id(commentId);

        List<CommentReply> replys = this.commentReplyMapper.select(record);
        List<CommentReplyAndUser> replyAndUser = new ArrayList<>();
        replys.forEach((reply)->{
            CommentReplyAndUser commentReplyAndUser = new CommentReplyAndUser();
            commentReplyAndUser.setCommentReply(reply);
            commentReplyAndUser.setUser(userService.queryUserByid(reply.getUser_id()));
            replyAndUser.add(commentReplyAndUser);
        });
        PageInfo<CommentReplyAndUser> pageInfo =  new PageInfo<>(replyAndUser);

        return new PageResult<>(pageInfo.getList(),pageInfo.getPages(),pageInfo.getTotal());
    }
//    public PageResult<CommentReply> selectCommentsReplyByPage(Long commentId, Integer page, Integer row) {
//        PageHelper.startPage(page,row);
//
//        CommentReply record = new CommentReply();
//        record.setReply_id(commentId);
//
//        List<CommentReply> replys = this.commentReplyMapper.select(record);
//        PageInfo<CommentReply> pageInfo =  new PageInfo<>(replys);
//
//        return new PageResult<>(pageInfo.getList(),pageInfo.getPages(),pageInfo.getTotal());
//    }
}
