package fun.ticsmyc.tmall.item.controller;

import fun.ticsmyc.tmall.common.pojo.PageResult;
import fun.ticsmyc.tmall.item.dto.CommentAndUser;
import fun.ticsmyc.tmall.item.dto.CommentReplyAndUser;
import fun.ticsmyc.tmall.item.pojo.Comment;
import fun.ticsmyc.tmall.item.pojo.CommentReply;
import fun.ticsmyc.tmall.item.service.CommentRedisService;
import fun.ticsmyc.tmall.item.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 评论
 * @author Ticsmyc
 * @date 2020-05-26 11:47
 */
@Controller
@RequestMapping("comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentRedisService commentRedisService;


    /**
     * 插入一条评论 【缓存+异步插入】
     * @param comment
     * @return
     */
    @PostMapping
    public ResponseEntity<Void> insertComment(Comment comment){
        if(comment == null || comment.getGoodsId()==null || comment.getUserId()==null){
            return ResponseEntity.badRequest().build();
        }
        this.commentRedisService.insertComment(comment);
        return ResponseEntity.ok().build();
    }


    /**
     * 回复一条评论
     * @return
     */
    @PostMapping("/reply")
    public ResponseEntity<Void> replyComment(CommentReply commentReply){
        if(commentReply == null || commentReply.getReply_id()==null || commentReply.getUser_id()==null){
            return ResponseEntity.badRequest().build();
        }
        this.commentRedisService.insertReplyComment(commentReply);
        return ResponseEntity.ok().build();
    }

    /**
     * 根据商品id 分页查询评论 （返回的数据包括了用户信息）
     * @param page
     * @param row
     * @param goodsId
     * @return
     */
//    @GetMapping("/{goodsId}")
//    public ResponseEntity<PageResult<Comment>> selectCommentsByPage(
//            @RequestParam(name = "page",defaultValue = "1")Integer page,
//            @RequestParam(name="row",defaultValue = "5")Integer row,
//            @PathVariable("goodsId")Long goodsId,
//            @RequestParam(name="replyTo",required = false)Long replyTo
//    ){
//        if(goodsId == null){
//            return ResponseEntity.badRequest().build();
//        }
//        PageResult<Comment> comments =this.commentRedisService.selectCommentsByPage(goodsId,page,row,replyTo);
//        return ResponseEntity.ok(comments);
//    }
    @GetMapping("/{goodsId}")
    public ResponseEntity<PageResult<CommentAndUser>> selectCommentsByPage(
            @RequestParam(name = "page",defaultValue = "1")Integer page,
            @RequestParam(name="row",defaultValue = "5")Integer row,
            @PathVariable("goodsId")Long goodsId,
            @RequestParam(name="replyTo",required = false)Long replyTo
    ){
        if(goodsId == null){
            return ResponseEntity.badRequest().build();
        }
        PageResult<CommentAndUser> comments =this.commentRedisService.selectCommentsByPage(goodsId,page,row,replyTo);
        return ResponseEntity.ok(comments);
    }

    /**
     * 修改评论
     * @param commentId
     * @param content
     * @return
     */
    @PutMapping
    public ResponseEntity<Void> modifyCommentById(@RequestParam("commentId") Long commentId, @RequestParam("content")String content){
        if(commentId == null || content == null){
            return ResponseEntity.badRequest().build();
        }
        this.commentRedisService.modifyCommentById(commentId,content);
        return ResponseEntity.ok().build();
    }

    /**
     * 根据评论id删除评论
     * @param commentId
     * @return
     */
    @DeleteMapping("/{commentId}")
    public ResponseEntity<Void> deleteCommentById(@PathVariable("commentId") Long commentId){
        if(commentId == null){
            return ResponseEntity.badRequest().build();
        }
        this.commentRedisService.deleteCommentById(commentId);
        return ResponseEntity.ok().build();
    }

    /**
     * 删除回复
     * @param replyId
     * @return
     */
    @DeleteMapping("/reply/{replyId}")
    public ResponseEntity<Void> deleteCommentReplyById(@PathVariable("replyId")Long replyId){
        if(replyId == null){
            return ResponseEntity.badRequest().build();
        }
        this.commentRedisService.deleteCommentReplyById(replyId);
        return ResponseEntity.ok().build();
    }

    /**
     * 分页查询评论回复
     * @param page
     * @param row
     * @param commentId
     * @return
     */
    @GetMapping("/reply/{commentId}")
    public ResponseEntity<PageResult<CommentReplyAndUser>> selectCommentsReplyByPage(
            @RequestParam(name = "page",defaultValue = "1")Integer page,
            @RequestParam(name="row",defaultValue = "5")Integer row,
            @PathVariable("commentId")Long commentId
    ){
        if(commentId == null){
            return ResponseEntity.badRequest().build();
        }
        PageResult<CommentReplyAndUser> replys =this.commentRedisService.selectCommentsReplyByPage(commentId,page,row);
        return ResponseEntity.ok(replys);
    }
//    @GetMapping("/reply/{commentId}")
//    public ResponseEntity<PageResult<CommentReply>> selectCommentsReplyByPage(
//            @RequestParam(name = "page",defaultValue = "1")Integer page,
//            @RequestParam(name="row",defaultValue = "5")Integer row,
//            @PathVariable("commentId")Long commentId
//    ){
//        if(commentId == null){
//            return ResponseEntity.badRequest().build();
//        }
//        PageResult<CommentReply> replys =this.commentRedisService.selectCommentsReplyByPage(commentId,page,row);
//        return ResponseEntity.ok(replys);
//    }
}
