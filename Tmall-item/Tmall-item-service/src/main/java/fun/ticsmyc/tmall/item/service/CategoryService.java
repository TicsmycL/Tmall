package fun.ticsmyc.tmall.item.service;

import fun.ticsmyc.tmall.item.mapper.CategoryMapper;
import fun.ticsmyc.tmall.item.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ticsmyc
 * @date 2020-04-29 21:45
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;


    public List<Category> queryCategoryByPid(Long pid) {
        Category record = new Category();
        record.setPid(pid);
        List<Category> categories = this.categoryMapper.select(record);
        return categories;
    }

    public List<String> queryCategoryNamesByIds(List<Long> ids) {
        List<Category> categories = this.categoryMapper.selectByIdList(ids);
        return categories.stream().map(category->category.getName()).collect(Collectors.toList());
    }
}
