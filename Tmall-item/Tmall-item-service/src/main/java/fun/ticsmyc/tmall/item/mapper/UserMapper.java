package fun.ticsmyc.tmall.item.mapper;

import fun.ticsmyc.tmall.item.pojo.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Ticsmyc
 * @date 2020-05-02 21:40
 */
public interface UserMapper extends Mapper<User> {
}
