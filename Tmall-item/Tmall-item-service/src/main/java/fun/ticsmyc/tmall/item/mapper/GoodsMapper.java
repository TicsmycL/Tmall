package fun.ticsmyc.tmall.item.mapper;

import fun.ticsmyc.tmall.item.pojo.Goods;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Ticsmyc
 * @date 2020-04-30 19:05
 */
public interface GoodsMapper extends Mapper<Goods> {

}
