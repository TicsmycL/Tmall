package fun.ticsmyc.tmall.item;

import fun.ticsmyc.rpc.EnableTRPC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Ticsmyc
 * @date 2020-04-29 21:57
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("fun.ticsmyc.tmall.item.mapper")
@EnableTRPC
public class TmallItemApplication {
    public static void main(String[] args) {
        SpringApplication.run(TmallItemApplication.class);
    }
}
