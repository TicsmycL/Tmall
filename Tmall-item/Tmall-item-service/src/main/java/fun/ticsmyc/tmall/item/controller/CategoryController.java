package fun.ticsmyc.tmall.item.controller;

import fun.ticsmyc.tmall.item.pojo.Category;
import fun.ticsmyc.tmall.item.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 商品分类列表
 * @author Ticsmyc
 * @date 2020-04-29 21:45
 */
@Controller
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 根据父分类ID查询子分类列表
     * @return
     */
    @GetMapping("list")
    public ResponseEntity<List<Category>> queryCategoryByPid(@RequestParam(value = "pid",defaultValue = "0")Long pid){
        if(pid == null || pid<0){
            //参数不合法
            return ResponseEntity.badRequest().build();
        }

        List<Category> categories =this.categoryService.queryCategoryByPid(pid);

        if(CollectionUtils.isEmpty(categories)){
            //子分类为空
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(categories);
    }

    @GetMapping
    public ResponseEntity<List<String>> queryCategoryNamesByIds(@RequestParam(value="id")List<Long> ids){
        List<String> names  =this.categoryService.queryCategoryNamesByIds(ids);
        if(CollectionUtils.isEmpty(names)){
            //查询失败
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(names);
    }
}
