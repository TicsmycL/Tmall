package fun.ticsmyc.tmall.item.controller;

import fun.ticsmyc.tmall.common.pojo.PageResult;
import fun.ticsmyc.tmall.item.pojo.Goods;
import fun.ticsmyc.tmall.item.service.GoodsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品
 * @author Ticsmyc
 * @date 2020-04-30 19:04
 */
@Controller
@RequestMapping("goods")
public class GoodsController {

    @Autowired
    private GoodsServiceImpl goodsService;

    /**
     * 分页查询所有商品
     * @return
     */
    @GetMapping
    public ResponseEntity<PageResult<Goods>> queryAllItems(
            @RequestParam(value = "page",defaultValue = "1")Integer page,
            @RequestParam(value="rows",defaultValue = "5") Integer rows,
            @RequestParam(value="status",required = false) Integer status
    ){
        PageResult<Goods> items = this.goodsService.queryAllItem(page,rows,status);
        return ResponseEntity.ok(items);
    }

    /**
     * 根据商品id查询商品信息
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<Goods> queryItemById(@PathVariable("id")Long id){
        if(id ==null){
            return ResponseEntity.badRequest().build();
        }
        Goods goods = this.goodsService.queryItemById(id);
        if(goods == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(goods);
    }

    /**
     * 根据商品类别分页查询商品信息
     * @return
     */
    @GetMapping("cid/{cid}")
    public ResponseEntity<PageResult<Goods>> queryItemsByCid(
            @PathVariable("cid")Long cid,
            @RequestParam(value = "page",defaultValue = "1")Integer page,
            @RequestParam(value="rows",defaultValue = "5") Integer rows,
            @RequestParam(value="status",required = false) Integer status
    ){
        if(cid ==null){
            return ResponseEntity.badRequest().build();
        }
        PageResult<Goods> items = this.goodsService.queryItemByCid(cid,page,rows,status);
        if(CollectionUtils.isEmpty(items.getItems())){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(items);
    }

    /**
     * 分页查询 【我发布的商品】
     * @param userId
     * @return
     */
    @GetMapping("/user/{userid}")
    public ResponseEntity<PageResult<List<Goods>>> selectItemsByUserId(
            @PathVariable("userid")Long userId,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name="rows",defaultValue = "5")Integer rows,
            @RequestParam(name="status",required = false)Integer status
    ){
        if(userId == null){
            return ResponseEntity.badRequest().build();
        }
        PageResult<List<Goods>> goods = this.goodsService.selectItemsByUserId(userId,page,rows,status);
        return ResponseEntity.ok(goods);
    }

    /**
     * 添加商品
     * @param goods
     * @return
     */
    @PostMapping
    public ResponseEntity<Void> addGoods(@RequestBody Goods goods){
        if(goods == null){
            return ResponseEntity.badRequest().build();
        }
        this.goodsService.addGoods(goods);
        return ResponseEntity.ok().build();
    }

    /**
     * 修改商品
     * @param goods
     * @return
     */
    @PutMapping
    public ResponseEntity<Void> modifyGoods(@RequestBody Goods goods){
        if(goods == null){
            return ResponseEntity.badRequest().build();
        }
        this.goodsService.modifyGoods(goods);
        return ResponseEntity.ok().build();
    }

    /**
     * 根据商品id删除商品
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteGoods(@PathVariable("id")Long id){
        if(id == null){
            return ResponseEntity.badRequest().build();
        }
        this.goodsService.deleteGoods(id);
        return ResponseEntity.ok().build();
    }

    /**
     * 更新商品状态
     * @param id
     * @param status
     * @return
     */
    @PutMapping("status")
    public ResponseEntity<Void> updateGoodsStatus(@RequestParam("id")Long id , @RequestParam("status")Integer status){
        if(id ==null || status == null){
            return ResponseEntity.badRequest().build();
        }
        this.goodsService.updateGoodsStatus(id,status);
        return ResponseEntity.ok().build();
    }

    @PostMapping("search")
    public ResponseEntity<PageResult<Goods>> searchGoods(
            @RequestParam("content") String content,
            @RequestParam(name="page",defaultValue = "1")Integer page,
            @RequestParam(name="row",defaultValue = "5")Integer row
    ){
        if(content == null){
            return ResponseEntity.badRequest().build();
        }
        PageResult<Goods> goods = this.goodsService.searchGoods(content,page,row);
        return ResponseEntity.ok(goods);
    }

}
