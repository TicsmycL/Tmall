package fun.ticsmyc.tmall.item.mapper;

import fun.ticsmyc.tmall.item.pojo.Comment;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Ticsmyc
 * @date 2020-05-26 11:47
 */
public interface CommentMapper extends Mapper<Comment> {

    @Update("UPDATE comment SET content = #{newContent} WHERE id=#{id}")
    int updateCommentById(Long id, String newContent);
}
