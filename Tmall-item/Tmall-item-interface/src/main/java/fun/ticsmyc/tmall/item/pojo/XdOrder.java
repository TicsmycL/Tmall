package fun.ticsmyc.tmall.item.pojo;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author Ticsmyc
 * @date 2020-05-14 9:53
 */
@Table(name="xd_order")
public class XdOrder {
    @Id
    private Long orderId;
    private Long goodsId;
    private Long goodsBuyerId;
    private Date createTime;
    private Integer order_status;


    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getGoodsBuyerId() {
        return goodsBuyerId;
    }

    public void setGoodsBuyerId(Long goodsBuyerId) {
        this.goodsBuyerId = goodsBuyerId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
