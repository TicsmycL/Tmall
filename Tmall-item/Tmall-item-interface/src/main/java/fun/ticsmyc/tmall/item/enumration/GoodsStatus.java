package fun.ticsmyc.tmall.item.enumration;

import org.springframework.lang.Nullable;

/**
 * @author Ticsmyc
 * @date 2020-05-14 14:07
 */
public enum GoodsStatus {
    ON_SALE(1,"在架上"),
    BOOK(2,"被预定"),
    FINISH(3,"已售出");

    private final int value;

    private final String reasonPhrase;

    GoodsStatus(int value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    public int value() {
        return this.value;
    }

    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }


    public static GoodsStatus valueOf(int statusCode) {
        GoodsStatus status = resolve(statusCode);
        if (status == null) {
            throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
        }
        return status;
    }

    @Nullable
    public static GoodsStatus resolve(int statusCode) {
        for (GoodsStatus status : values()) {
            if (status.value == statusCode) {
                return status;
            }
        }
        return null;
    }
}
