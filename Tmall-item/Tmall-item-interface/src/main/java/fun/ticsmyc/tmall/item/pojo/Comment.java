package fun.ticsmyc.tmall.item.pojo;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author Ticsmyc
 * @date 2020-05-26 11:41
 */
@Table(name="comment")
public class Comment {

    @Id
    private Long id;
    private Long goodsId;
    private Date createTime;
    private Long userId;
    private String content;


    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", goodsId=" + goodsId +
                ", createTime=" + createTime +
                ", userId=" + userId +
                ", content='" + content + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
