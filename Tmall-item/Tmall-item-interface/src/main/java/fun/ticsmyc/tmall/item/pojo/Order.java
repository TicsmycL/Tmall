package fun.ticsmyc.tmall.item.pojo;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Ticsmyc
 * @date 2020-08-10 10:54
 */
@Table(name="`order`")
public class Order implements Serializable{

    /**
     * 订单号
     */
    @Id
    private Long id;
    /**
     * 商品id
     */
    private Long goodsId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 价格
     */
    private Double price;

    /**
     * 订单状态
     */
    private String state;

    /**
     * 购买时间
     */
    private Date createTime;
    /**
     * 支付信息
     */
    private String payment;
    /**
     * 备注信息
     */
    private String remark;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", goodsId=" + goodsId +
                ", userId='" + userId + '\'' +
                ", price=" + price +
                ", state='" + state + '\'' +
                ", createTime=" + createTime +
                ", payment='" + payment + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
