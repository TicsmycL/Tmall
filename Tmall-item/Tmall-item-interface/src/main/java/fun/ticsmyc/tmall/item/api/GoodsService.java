package fun.ticsmyc.tmall.item.api;

import fun.ticsmyc.rpc.server.annotation.TRPCInterface;
import fun.ticsmyc.tmall.common.pojo.PageResult;
import fun.ticsmyc.tmall.item.pojo.Goods;

import java.util.List;

/**
 * @author Ticsmyc
 * @date 2020-12-01 16:05
 */
@TRPCInterface
public interface GoodsService {

    /**
     * 根据商品类别查询商品信息
     * @param cid
     * @return
     */
    public PageResult<Goods> queryItemByCid(Long cid, Integer page, Integer rows, Integer status);


    /**
     * 添加商品
     * @param goods
     */
    public void addGoods(Goods goods);

    /**
     * 修改商品
     * @param goods
     */
    public void modifyGoods(Goods goods);

    /**
     * 删除商品
     * @param id
     */
    public void deleteGoods(Long id);

    /**
     * 根据id查询商品
     * @param id
     * @return
     */
    public Goods queryItemById(Long id);

    /**
     * 查询所有商品
     * @return
     */
    public PageResult<Goods> queryAllItem(Integer page, Integer rows,Integer status);

    /**
     * 更新商品状态
     * @param goodsId
     * @param status
     */
    public void updateGoodsStatus(Long goodsId , Integer status);

    /**
     * 根据用户id  查询该用户发布的商品
     * @param userId
     * @return
     */
    public PageResult<List<Goods>> selectItemsByUserId(Long userId, Integer page, Integer rows, Integer status);

    /**
     * 搜索， 结果分页
     * @param content
     * @param page
     * @param row
     */
    public PageResult<Goods> searchGoods(String content,Integer page,Integer row);


}
