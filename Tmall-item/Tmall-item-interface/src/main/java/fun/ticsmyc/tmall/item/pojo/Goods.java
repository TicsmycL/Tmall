package fun.ticsmyc.tmall.item.pojo;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author Ticsmyc
 * @date 2020-04-29 21:39
 */
@Table(name="item")
public class Goods {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String images;
    private Long cid;
    private Date createtime;
    private String description;
    private Long userid;
    private String quhuofangshi;
    private Double price;
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getQuhuofangshi() {
        return quhuofangshi;
    }

    public void setQuhuofangshi(String quhuofangshi) {

        this.quhuofangshi = quhuofangshi;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", images='" + images + '\'' +
                ", cid=" + cid +
                ", createtime=" + createtime +
                ", description='" + description + '\'' +
                ", userid=" + userid +
                ", quhuofangshi='" + quhuofangshi + '\'' +
                ", price=" + price +
                ", status=" + status +
                '}';
    }
}
