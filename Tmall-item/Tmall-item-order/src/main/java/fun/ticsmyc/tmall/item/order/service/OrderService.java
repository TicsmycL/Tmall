package fun.ticsmyc.tmall.item.order.service;

import fun.ticsmyc.tmall.item.pojo.Order;

import java.util.List;

/**
 * @author Ticsmyc
 * @date 2020-11-30 17:06
 */
public interface OrderService {

    public Long createOrder(Order order);

    public Boolean finishPayment(Long id);

    public List<Order> getOrderByUserId(String userId, Integer state);

    public Order getOrderByOrderId(Long id);

    public Order getOrderFromMysqlByOrderId(Long id);

    public void deleteOrderInRedis(Long id,String userId);

    public void deleteOutstandingOrder(Long id,String userId);

    public void insertOrder(Order order);

}
