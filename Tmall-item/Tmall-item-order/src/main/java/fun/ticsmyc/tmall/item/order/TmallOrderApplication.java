package fun.ticsmyc.tmall.item.order;

import fun.ticsmyc.rpc.EnableTRPC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("fun.ticsmyc.tmall.item.order.mapper")
@EnableTransactionManagement
@EnableCaching
@EnableEurekaClient
@EnableTRPC
public class TmallOrderApplication   {


    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(TmallOrderApplication.class, args);

    }



}
