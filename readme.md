

# 分布式微服务的二手商城

> 接口文档：
> https://docs.apipost.cn/view/2e7a3c1ab7939a3d

服务间的远程调用使用了自己造的轮子：[T_RPC_Framework](https://gitee.com/TicsmycL/t-rpc-framework)
## 部署
- XDmall-item-service、XDmall-sms、XDmall-upload 这三个微服务的配置文件需要自己填一下（MySql密码、Redis地址、RabbitMQ地址等）
- 每次必须开启的微服务： XDmall-registry 注册中心 、 XDmall-gateway网关 、XDmall-item业务

### 端口使用情况

| 服务名   | 端口号 |
| -------- | ------ |
| 注册中心 | 10086  |
| 网关     | 10010  |
| 上传服务 | 8082   |
| 实体服务 | 8083   |
| 订单服务 | 8084   |





## 介绍

![微服务架构图](pic/微服务架构图.png)

### 技术栈

- SpringBoot
- SpringCloud
- MyBatis
- RabbitMQ
- Redis & Redisson
- Nginx
- FastDFS

### 一些优化

1. 对于写密集型业务，如用户评论的发布和回复，使用Redis缓存与RabbitMQ消息队列配合，实现主数据库MySql异步写入。
2. 使用Redisson的RedLock分布式锁保证分布式系统中的数据一致性，避免数据重复缓存。
3. 邮件通知使用消息队列解耦成一个单独的服务，原因如下：一是为了保证微服务的单一职责；二是利用了消息队列削峰的特性，避免同时处理大量请求。

